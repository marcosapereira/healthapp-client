# HealthApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.6.

## Usage

* Install

```shell
$ npm install 
```

* Start

```shell
$ npm start
# or
$ ng serve --host 0.0.0.0 --disable-host-check
```

Then try to open a browser window and paste `localhost:4200/patients` or `localhost:4200/doctors` or `localhost:4200/appointments`.

---

## Ponte recommendations and notes

### Tools

* **SonarLint:** is a free IDE extension that lets you fix coding issues before they exist! Like a spell checker, SonarLint highlights Bugs and Security Vulnerabilities as you write code, with clear remediation guidance so you can fix them before the code is even committed. SonarLint in VS Code supports analysis of JavaScript, TypeScript, Python, Java, HTML & PHP code, and you can install it directly from the VS Code Marketplace!

* SonarQube

### Notes

* Aquando do _build_ inicial, no **routing**, carrega e valida todos os imports e DI na altura de compilação. Valida o que está em declarations, imports, providers.

* O component deve ser responsável apenas por gerir o HTML, abstraindo tudo o resto para serviços.

---

## Classes

* [05/02/2021](aulas/dia05022021.md)
* [12/03/2021](aulas/dia12032021.md)