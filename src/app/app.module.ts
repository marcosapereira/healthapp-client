import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterComponent } from './components/layouts/footer/footer.component';
import { APP_ROUTES } from './app.routing';
import { RouterModule } from '@angular/router';
import { SharedModule } from './shared.module';
import { PatientModule } from './components/patient/patient.module';
import { DoctorModule } from './components/doctor/doctor.module';
import { AppointmentModule } from './components/appointment/appointment.module';
import { FakeInterceptor } from './core/interceptors/fake.interceptor';
import { registerLocaleData } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import localePt from '@angular/common/locales/pt';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavComponent } from './components/layouts/nav-layout/nav.component';
import { NonavComponent } from './components/layouts/nonav-layout/nonav.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';

registerLocaleData(localePt);

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		NonavComponent,
		NavComponent,
		FooterComponent
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		NgxChartsModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		AppointmentModule,
		DoctorModule,
		PatientModule,
		SharedModule,
		RouterModule.forRoot(APP_ROUTES)
	],
	providers: [
		{ provide: LOCALE_ID, useValue: 'pt-PT' },
		{
			provide: HTTP_INTERCEPTORS,
			useClass: FakeInterceptor,
			multi: true
		}
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
