import { Routes } from '@angular/router';
import { APPOINTMENT_ROUTES } from './components/appointment/appointment.routing';
import { DOCTOR_ROUTES } from './components/doctor/doctor.routing';
import { PATIENT_ROUTES } from './components/patient/patient.routing';
import { HomeComponent } from './components/home/home.component';
import { NavComponent } from './components/layouts/nav-layout/nav.component';
import { NonavComponent } from './components/layouts/nonav-layout/nonav.component';
import { AuthorizationGuard } from './core/guards/authorization.guard';
import { UserResolver } from './core/resolvers/user.resolver';

export const APP_ROUTES: Routes = [
	{ path: '', redirectTo: '/login', pathMatch: 'full' },
	{
		path: '',
		component: NonavComponent,
        /**
         * com esta resolucao da promise, estamos a dizer ao Angular que não queremos carregar o
         * LoginModule logo, mas sim apenas quando aceder a esta rota
         */
        loadChildren: () => import('./components/login/login.module').then(m => m.LoginModule)
	},
	{
		path: '',
		component: NavComponent,
		resolve: { user: UserResolver },
		canActivate: [AuthorizationGuard],
		children: [
			{ path: 'home', component: HomeComponent },
			...APPOINTMENT_ROUTES,
			...DOCTOR_ROUTES,
			...PATIENT_ROUTES
		]
	},
	{ path: '**', redirectTo: '' }
];