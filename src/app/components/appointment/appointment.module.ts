import { SharedModule } from './../../shared.module';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AppointmentListComponent } from './../appointment/list/appointment-list.component';
import { AppointmentDetailComponent } from './../appointment/detail/appointment-detail.component';
import { AppointmentCreateComponent } from './../appointment/create/appointment-create.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';

@NgModule({
    declarations: [
        AppointmentCreateComponent,
        AppointmentDetailComponent,
        AppointmentListComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        FontAwesomeModule,
        SharedModule,
        MatTableModule,
		MatFormFieldModule,
		MatInputModule,
		MatButtonModule,
		MatGridListModule,
    ]
})
export class AppointmentModule {}