import { AppointmentDetailComponent } from './detail/appointment-detail.component';
import { AppointmentCreateComponent } from './create/appointment-create.component';
import { AppointmentListComponent } from './list/appointment-list.component';
import { Routes } from '@angular/router';

export const APPOINTMENT_ROUTES: Routes = [
    { path: 'appointments', component: AppointmentListComponent },
    { path: 'appointments/create', component: AppointmentCreateComponent },
    { path: 'appointments/:id', component: AppointmentDetailComponent }
];