import { SharedModule } from './../../shared.module';
import { DoctorListComponent } from './list/doctor-list.component';
import { DoctorCreateComponent } from './create/doctor-create.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';

@NgModule({
    declarations: [
        DoctorCreateComponent,
        DoctorListComponent
    ],
    imports: [
        CommonModule,
        RouterModule,
        BrowserAnimationsModule,
        SharedModule,
        MatTableModule,
		MatFormFieldModule,
		MatInputModule,
		MatButtonModule,
		MatGridListModule,
    ]
})
export class DoctorModule { }