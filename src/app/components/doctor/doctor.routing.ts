
import { Routes } from '@angular/router';
import { DoctorCreateComponent } from './create/doctor-create.component';
import { DoctorDetailComponent } from './detail/doctor-detail.component';
import { DoctorListComponent } from './list/doctor-list.component';

export const DOCTOR_ROUTES: Routes = [
    { path: 'doctors', component: DoctorListComponent },
    { path: 'doctors/create', component: DoctorCreateComponent },
    { path: 'doctors/:id', component: DoctorDetailComponent },
];