import { Speciality } from './../../constants/speciality.constants';
import { MetricService } from './../../services/metric.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Pair } from 'src/app/entities/pair';

@Component({
    selector: 'home-root',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent {

    specialitiesCount: Pair[] = [];
    genderCount: Pair[] = [];
    view: any[] = [800, 400];
    colorScheme = { domain: ['red', 'green', 'blue'] };

    constructor(
        private _route: Router,
        private _metricService: MetricService
    ) {
        this._metricService.getSpecialitiesCount().subscribe(data => {
            for (const key in data) {
                this.specialitiesCount.push(new Pair(Speciality[key], data[key]));
            }
            this.specialitiesCount = [...this.specialitiesCount];
            console.log(this.specialitiesCount);
        });

    }

    goToPatients() {
        this._route.navigate(['patients'])
    }

    goToDoctors() {
        this._route.navigate(['doctors'])
    }

    goToAppointments() {
        this._route.navigate(['appointments'])
    }

    onResize(event) {
        this.view = [event.target.innerWidth / 2.4, 400];
    }





}
