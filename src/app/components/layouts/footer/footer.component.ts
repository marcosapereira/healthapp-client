import { Component } from '@angular/core';

@Component({
    selector: 'ze-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
    text = 'Lopes da Silva';
}