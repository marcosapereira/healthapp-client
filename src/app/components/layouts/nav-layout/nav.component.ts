import { User } from 'src/app/entities/user';
import { AuthenticationService } from './../../../services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'nav-layout',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
    title = 'Health Application';
    user: User;

    constructor(
        private _authService: AuthenticationService,
        private _route: ActivatedRoute) {}

    ngOnInit(): void {
        this._route.data.subscribe( data => {
            this.user = data.user;
        });
    }

    logOut(): void {
        this._authService.logOut();
    }
}
