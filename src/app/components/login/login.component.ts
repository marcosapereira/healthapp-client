import { AuthenticationService } from './../../services/authentication.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/entities/user';

@Component({
    selector: 'login-root',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {

    username: string;
    password: string;

    constructor(
        private _route: Router,
        private _authService: AuthenticationService) { }

    login() {
        console.log('login(' + this.username + ', ' + this.password + ')');
        this._authService.authenticate(this.username, this.password).subscribe(
            (user: User) => {
                if (user != null) {
                    localStorage.setItem('token', user.token);
                    localStorage.setItem('userId', '' + user.id);
                    this._route.navigate(['home']);
                } else {
                    alert('Wrong username or password');
                }
            }
        );

    }

}
