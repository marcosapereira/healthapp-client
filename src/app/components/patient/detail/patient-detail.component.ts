import { Patient } from './../../../entities/patient';
import { PatientService } from './../../../services/patient.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Gender } from 'src/app/constants/gender.constants';

@Component({
    selector: 'app-patient-detail',
    templateUrl: './patient-detail.component.html',
    styleUrls: ['./patient-detail.component.scss']
})
export class PatientDetailComponent implements OnInit {

    Gender = Gender;
    patientForm: FormGroup;
    isCreateMode: boolean;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _patientService: PatientService
    ) { }

    ngOnInit(): void {
        const patientId = this._route.snapshot.paramMap.get('id');

        if (patientId) {
            this.isCreateMode = false;
            this._patientService.getPatient(patientId).subscribe(patient => {
                this.setPatientForm(patient);
            });
        }
        else {
            this.isCreateMode = true;
            this.setPatientForm();
            this.patientForm = new FormGroup({
                name: new FormControl(null, [Validators.required, Validators.minLength(5), this.forbiddenNameValidator(new RegExp('Marco'))]),
                gender: new FormControl(null),
                birthdate: new FormControl(null),
                phoneNumber: new FormControl(null),
                address: new FormControl(null),
                citizenCard: new FormControl(null),
                fiscalNumber: new FormControl(null),
                socialSecurityNumber: new FormControl(null),
                insuranceNumber: new FormControl(null)
            });
        }
    }

    savePatient(patient: Patient): void {
        patient.birthdate = new Date();
        if (this.isCreateMode) {
            this._patientService.createPatient(patient).subscribe(newPatient => {
                console.log(newPatient);
            })
        } else {
            this._patientService.updatePatient(patient).subscribe(updatedPatient => {
                console.log(updatedPatient);
            })
        }
        this._router.navigateByUrl('/patients');
    }

    setPatientForm(patient?): void {
        this.patientForm = new FormGroup({
            name: new FormControl(patient?.name || null, [Validators.required, Validators.minLength(5), this.forbiddenNameValidator(new RegExp('Marco'))]),
            gender: new FormControl(patient?.gender || null),
            birthdate: new FormControl(patient?.birthdate || null),
            phoneNumber: new FormControl(patient?.phoneNumber || null),
            address: new FormControl(patient?.address || null),
            citizenCard: new FormControl(patient?.citizenCard || null),
            fiscalNumber: new FormControl(patient?.fiscalNumber || null),
            socialSecurityNumber: new FormControl(patient?.socialSecurityNumber || null),
            insuranceNumber: new FormControl(patient?.insuranceNumber || null)
        });
    }

    get name() { return this.patientForm.get('name'); }

    /**
     * 
     * @param nameRe regular expression
     * @returns a function that executes when user performs an input update and returns a boolean
     */
    forbiddenNameValidator(nameRe: RegExp): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } | null => {
            const forbidden = nameRe.test(control.value);
            return forbidden ? { forbiddenName: { value: control.value } } : null;
        };

    }

}
