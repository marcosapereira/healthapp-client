import { Component, OnInit } from '@angular/core';
import { Patient } from '../../../entities/patient';
import { PatientService } from '../../../services/patient.service';

@Component({
    selector: 'app-patient-list',
    templateUrl: './patient-list.component.html',
    styleUrls: ['./patient-list.component.scss']
})
export class PatientListComponent implements OnInit {

    patients: Patient[] = [];

    constructor(private _patientService: PatientService) {

    }

    ngOnInit() {
        this.getPatientList();
    }

    deletePatient(id: string) {
        this._patientService.deletePatient(id).subscribe(_ => {
            console.log(`Patient with id ${id} deleted!`);
            // refresh list
            this.getPatientList();
        });
    }

    getPatientList() {
        this._patientService.getPatientList().subscribe(
            (res: Patient[]) => {
                this.patients = res;
            });
    }

}
