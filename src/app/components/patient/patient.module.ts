import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from './../../shared.module';
import { PatientListComponent } from './list/patient-list.component';
import { PatientDetailComponent } from './detail/patient-detail.component';
import { PatientCreateComponent } from './create/patient-create.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        PatientCreateComponent,
        PatientDetailComponent,
        PatientListComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule,
        SharedModule,
    ]
})
export class PatientModule { }