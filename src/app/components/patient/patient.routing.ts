import { Routes } from '@angular/router';
import { UserResolver } from './../../core/resolvers/user.resolver';
import { PatientCreateComponent } from './create/patient-create.component';
import { PatientDetailComponent } from './detail/patient-detail.component';
import { PatientListComponent } from './list/patient-list.component';

export const PATIENT_ROUTES: Routes = [
    { path: 'patients', component: PatientListComponent, resolve: { user: UserResolver } },
    { path: 'patients/create', component: PatientDetailComponent },
    { path: 'patients/:id', component: PatientDetailComponent },
];