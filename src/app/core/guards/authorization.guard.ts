import { AuthenticationService } from './../../services/authentication.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable( {providedIn: 'root'})
export class AuthorizationGuard implements CanActivate {

    constructor(
        private _router: Router,
        private _authService: AuthenticationService) {}

    canActivate(): boolean {
        const isAuthorized = this._authService.isAuthenticated();
        if (!isAuthorized) {
            this._router.navigate(['login']);
        }
        return isAuthorized;

    }
}