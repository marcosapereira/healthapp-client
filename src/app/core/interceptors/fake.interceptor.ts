import { users } from './../../mocks/users';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User } from 'src/app/entities/user';

@Injectable()
export class FakeInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const { url, method, body } = req;
        if (url.endsWith('/login') && method === 'POST') {
            const { username, password } = body;
            const user = users.find(u => u.username === username && u.password === password);
            if (user) {
                return of(new HttpResponse({ status: 200, body: user }));
            }
            return of(new HttpResponse({ status: 401, body: null }));
        }
        else if (url.match(/\/users\/\d+$/) && method === 'GET') {
            const urlParts = url.split('/');
            // tslint:disable-next-line:radix
            const id = parseInt(urlParts[urlParts.length - 1]);
            const user: User = users.find(u => u.id === id);
            return of(new HttpResponse({ status: 200, body: user }));
        }

        return next.handle(req);
    }
}
