import { UserService } from './../../services/user.service';
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';


@Injectable({ providedIn: 'root' })
export class UserResolver implements Resolve<any> {

    constructor(private _userService: UserService) {}

    resolve(): any {
        return this._userService.getUser(localStorage.getItem('userId'));
    }
}