import { ActivatedRoute } from '@angular/router';

// So quem tem permissão de RGPD é que pode ver o Insurance Number

import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { User } from '../entities/user';

@Directive({selector: '[hasPermission]'})
export class PermissionDirective implements OnInit {

    private permissions = [];
    private user: User;

    constructor(
        private _template: TemplateRef<any>,
        private _view: ViewContainerRef,
        private _route: ActivatedRoute) {}

    @Input() set hasPermission(prms: string[]) {
        // get permissions
        this.permissions = prms;
    }

    ngOnInit() {
        // getting context information
        this._route.data.subscribe(data => {
            this.user = data.user;
            this.updateView();
        });
    }

    private updateView() {
        if (this.checkPermission()) {
            this._view.createEmbeddedView(this._template);
        } else {
            this._view.clear();
        }
    }

    private checkPermission(): boolean {
        // validate the permission
        if(this.user != null) {
            for(const perm of this.permissions) {
                const permissionFound = this.user.permissions.find( prm => prm === perm);
                if (permissionFound != null) {
                    return true;
                }
            }
            return false;
        }
    }
}