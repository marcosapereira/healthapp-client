export class Pair {

    constructor(public name: string, public value: number) { }
}