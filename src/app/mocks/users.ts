import { User } from '../entities/user';

export let users: User[] = [
    {​​
        id: 12345, firstName: 'John', lastName: 'Doe',
        username: 'jdoe', password: 's3cr3t',
        token: '8924d8j23982813jdujjujd9q',
        permissions: ['RGPD']
    }​​,
    {​​
        id: 23456, firstName: 'Jane', lastName: 'Dan',
        username: 'jdan', password: 's3cr3t',
        token: 'j9889fjiwjrewij8jr982jje8', permissions: []
    }​,
    {​​
        id: 34567, firstName: 'Manuel', lastName: 'João',
        username: 'mjoao', password: 's3cr3t',
        token: 'j8qh3e2ji3d2924jd283jdi32', permissions: []
    }​​,
    {​​
        id: 45678, firstName: 'Joana', lastName: 'Manuela',
        username: 'jmanuela', password: 's3cr3t',
        token: 'iuyer842r923d3j28j984f84j', permissions: []
    }​​
];
