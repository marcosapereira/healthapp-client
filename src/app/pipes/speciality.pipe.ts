import { Speciality } from './../constants/speciality.constants';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'speciality'
})
export class SpecialityPipe implements PipeTransform {
    // colocar espaços de 3 em 3 algarismos do número de telefone
    // TODO: resolver esta lógica que não está a funcionar bem
    transform(speciality: string) {
        const specialities = Speciality;
        return specialities[speciality];
    }
}
