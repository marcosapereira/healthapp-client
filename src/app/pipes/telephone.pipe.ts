import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'telephone'
})
export class TelephonePipe implements PipeTransform {
    // colocar espaços de 3 em 3 algarismos do número de telefone
    transform(telephone: string) {
        const telephoneStr = telephone + '';
        let result = telephoneStr[0];
        for(let i = 1; i<telephoneStr.length; i++) {
            result += telephoneStr[i];
            if ((i+1)%3===0) { result+= ' '; }
        }
        return result;
    }
}
