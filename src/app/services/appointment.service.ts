import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Appointment } from '../entities/appointment';
import { catchError, tap } from 'rxjs/operators';
import { ErrorHandlingService } from './error-handling.service';

@Injectable({ providedIn: 'root' })
export class AppointmentService {

    private baseUrl = 'http://localhost:3000/api/appointments';

    constructor(
        private httpClient: HttpClient,
        private _errorHandlerService: ErrorHandlingService) { }

    async getAppointmentList() {
        return await this.httpClient.get<Appointment[]>(`${this.baseUrl}`).toPromise();
    }

    getAppointment(id: string) {
        return this.httpClient.get(`${this.baseUrl}/${id}`).pipe(
            tap(_ => console.log(`fetched appointment ${id}`)),
            catchError(this._errorHandlerService.handleError('geAppointment'))
        );
    }

    createAppointment(appointment: Appointment) {
        return this.httpClient.post(`${this.baseUrl}`, appointment).pipe(
            tap(_ => console.log(`fetched create appointment with doctor ${appointment.doctor}`)),
            catchError(this._errorHandlerService.handleError('createAppointment'))
        );
    }

    updateAppointment(appointment: Appointment) {
        return this.httpClient.put(`${this.baseUrl}/${appointment.id}`, appointment).pipe(
            tap(_ => console.log(`fetched update appointment ${appointment.id}`)),
            catchError(this._errorHandlerService.handleError('updateAppointment'))
        );
    }

    deleteAppointment(id: string) {
        return this.httpClient.delete(`${this.baseUrl}/${id}`).pipe(
            tap(_ => console.log(`fetched delete patient ${id}`)),
            catchError(this._errorHandlerService.handleError('deletePatient'))
        );
    }

}