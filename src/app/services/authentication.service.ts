import { User } from './../entities/user';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private baseUrl = 'http://localhost:3000/'

  constructor(private _httpClient: HttpClient) { }

  authenticate(username: string, password: string) {
    return this._httpClient.post<User>(`${this.baseUrl}/login`, {username, password});
  }

  isAuthenticated(): boolean {
    return localStorage.getItem('token') !== null;
  }

  logOut(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    window.location.reload();
  }
}
