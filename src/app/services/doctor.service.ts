import { ErrorHandlingService } from './error-handling.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Doctor } from '../entities/doctor';
import { catchError, tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class DoctorService {

    private baseUrl = 'http://localhost:3000/api/doctors';

    constructor(
        private httpClient: HttpClient,
        private _errorHandlerService: ErrorHandlingService) { }

    getDoctorList() {
        return this.httpClient.get<Doctor[]>(`${this.baseUrl}`).pipe(
            tap(_ => console.log(`fetched doctor  list`)),
            catchError(this._errorHandlerService.handleError('getDoctorList'))
        );
    }

    getDoctor(id: string) {
        return this.httpClient.get<Doctor>(`${this.baseUrl}/${id}`).pipe(
            tap(_ => console.log(`fetched doctor ${id}`)),
            catchError(this._errorHandlerService.handleError('getDoctor'))
        );
    }

    createDoctor(doctor: Doctor) {
        return this.httpClient.post(`${this.baseUrl}`, doctor).pipe(
            tap(_ => console.log(`fetched create doctor ${doctor.name}`)),
            catchError(this._errorHandlerService.handleError('createDoctor'))
        );
    }

    updateDoctor(doctor: Doctor) {
        return this.httpClient.put(`${this.baseUrl}/${doctor.id}`, doctor).pipe(
            tap(_ => console.log(`fetched update doctor ${doctor.id}`)),
            catchError(this._errorHandlerService.handleError('updateDoctor'))
        );
    }

    deleteDoctor(id: string) {
        return this.httpClient.delete(`${this.baseUrl}/${id}`).pipe(
            tap(_ => console.log(`fetched delete docgtor ${id}`)),
            catchError(this._errorHandlerService.handleError('deleteDoctor'))
        );
    }
}
