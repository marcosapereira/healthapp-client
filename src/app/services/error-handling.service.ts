import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({ providedIn: 'root'})
export class ErrorHandlingService {
    public handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
          console.error(error); // log to console instead
          this.log(`${operation} failed: ${error.message}`);
          return of(result);
        };
    }

    private log(arg0: string) {
        if (arg0) {
            console.error(arg0);
        }
        throw new Error('Method not implemented.');
    }
}