import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Doctor } from '../entities/doctor';

@Injectable({ providedIn: 'root' })
export class MetricService {

    private baseUrl = 'http://localhost:3000/api/metrics';

    constructor(
        private httpClient: HttpClient) { }

    getSpecialitiesCount() {
        return this.httpClient.get(`${this.baseUrl}/specialities/count`);
    }

}
