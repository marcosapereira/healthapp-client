import { ErrorHandlingService } from './error-handling.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Patient } from '../entities/patient';
import { catchError, tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class PatientService {

    private baseUrl = 'http://localhost:3000/api/patients';

    constructor(
        private httpClient: HttpClient,
        private _errorHandlerService: ErrorHandlingService) { }

    getPatientList() {
        return this.httpClient.get<Patient[]>(`${this.baseUrl}`).pipe(
            tap(_ => console.log(`fetched patient list`)),
            catchError(this._errorHandlerService.handleError('getPatientList'))
        );
    }

    getPatient(id: string) {
        return this.httpClient.get<Patient>(`${this.baseUrl}/${id}`).pipe(
            tap(_ => console.log(`fetched patient ${id}`)),
            catchError(this._errorHandlerService.handleError('getPatient'))
        );
    }

    createPatient(patient: Patient) {
        return this.httpClient.post(`${this.baseUrl}`, patient).pipe(
            tap(_ => console.log(`fetched create patient ${patient.name}`)),
            catchError(this._errorHandlerService.handleError('createPatient'))
        );
    }

    updatePatient(patient: Patient) {
        return this.httpClient.put(`${this.baseUrl}/${patient.id}`, patient).pipe(
            tap(_ => console.log(`fetched update patient ${patient.id}`)),
            catchError(this._errorHandlerService.handleError('updatePatient'))
        );
    }

    deletePatient(id: string) {
        return this.httpClient.delete(`${this.baseUrl}/${id}`).pipe(
            tap(_ => console.log(`fetched delete patient ${id}`)),
            catchError(this._errorHandlerService.handleError('deletePatient'))
        );
    }

    log(arg0: string) {
        throw new Error('Method not implemented.');
    }
}