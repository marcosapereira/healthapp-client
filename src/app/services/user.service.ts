import { User } from '../entities/user';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { users } from '../mocks/users';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = 'http://localhost:3000/api/users';

  constructor(private _httpClient: HttpClient) { }

  getUser(id: string): Observable<User> {
    return this._httpClient.get<User>(`${this.baseUrl}/${id}`).pipe(
      map((user: User) => { return user; })
    );
  }
}
