import { SpecialityPipe } from './pipes/speciality.pipe';
import { TelephonePipe } from './pipes/telephone.pipe';
import { PermissionDirective } from './directives/permission.directive';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [
        PermissionDirective,
        TelephonePipe,
        SpecialityPipe
    ],
    exports: [
        PermissionDirective,
        TelephonePipe,
        SpecialityPipe
    ]
})
export class SharedModule { }